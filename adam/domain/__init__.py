# -*- coding: utf-8 -*-
"""
    adam.domain
    ~~~~~~~~~~~

    this package exposes the API domain and commonly used settings.

    :copyright: (c) 2015 by Nicola Iarocci and CIR2000.
    :license: BSD, see LICENSE for more details.
"""
import countries
import companies
import user_accounts
import documents
import accounts
from dashboard import dashboard_accounts, dashboard_documents

DOMAIN = {
    'countries': countries.definition,
    'companies': companies.definition,
    'dashboard-accounts': dashboard_accounts.definition,
    'dashboard-documents': dashboard_documents.definition,
    'user_accounts': user_accounts.definition,
    'documents': documents.definition,
    'accounts': accounts.definition,
}
